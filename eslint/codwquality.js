/* eslint-disable max-classes-per-file */

const path = require('path');
const fs = require('fs');

/**
 * @interface ESLintMessage
 * @property {string} ruleId
 * @property {number} severity
 * @property {string} message
 * @property {number} line
 * @property {number} column
 * @property {string} nodeType
 * @property {number} endLine
 * @property {number} endColumn
 */

/**
 * @interface ESLintFile
 * @property {string} filePath
 * @property {ESLintMessage[]} messages
 */

/**
 * @interface CodeQualityItem
 * @property {string} description
 * @property {string} fingerprint
 * @property {string} severity
 * @property {string} location.path
 * @property {string} location.lines.begin
 */

/**
 */

class FilePath {
  /**
   * @param {string} value
   */
  constructor(value) {
    /** @private */
    this.value = value;
  }

  /**
   * @return {string}
   */
  relative() {
    return path.relative(process.cwd(), this.value);
  }
}

class SourceMessage {
  /**
   * @param {FilePath} filePath
   * @param {ESLintMessage} message
   */
  constructor(filePath, message) {
    /** @private */
    this.filePath = filePath;

    /** @private */
    this.message = message;
  }

  /**
   * @return {CodeQualityItem}
   */
  toCodeQuality() {
    return {
      description: this.message.message,
      severity: this.severity(),
      fingerprint: this.fingerprint(),
      location: {
        path: this.filePath.relative(),
        lines: {
          begin: this.message.line,
        },
      },
    };
  }

  /**
   * @private
   * @return {string}
   */
  severity() {
    switch (this.message.severity) {
      case 1: return 'minor';
      case 2: return 'critical';
      default: return 'info';
    }
  }

  /**
   * @private
   * @return {string}
   */
  fingerprint() {
    return `${this.filePath.relative()}:${this.message.line}:${this.message.ruleId}`;
  }
}

class SourceItem {
  /**
   * @param {ESLintFile} file
   */
  constructor(file) {
    /** @private */
    this.file = file;
  }

  /**
   * @return {boolean}
   */
  hasProblem() {
    return this.file.messages.length > 0;
  }

  /**
   * @return {CodeQualityItem[]}
   */
  toCodeQualities() {
    return this.messages().map((message) => message.toCodeQuality());
  }

  /**
   * @private
   * @return {FilePath}
   */
  filePath() {
    return new FilePath(this.file.filePath);
  }

  /**
   * @private
   * @return {SourceMessage[]}
   */
  messages() {
    return this.file.messages
      .map((message) => new SourceMessage(this.filePath(), message));
  }
}

/**
 * @param {ESLintFile[]} results
 * @return {string}
 */
module.exports = (results) => {
  const items = results.map((result) => new SourceItem(result))
    .filter((file) => file.hasProblem())
    .map((file) => file.toCodeQualities())
    .reduce((accumulator, current) => [...accumulator, ...current], []);

  const output = JSON.stringify(items, null, 2);
  fs.writeFileSync('codequality.json', output, 'utf8');
  return output;
};
