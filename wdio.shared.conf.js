/* eslint-disable no-unused-vars */
const path = require('path');

exports.config = {
  specs: [
    path.join(__dirname, '/tests/e2e/**/*.spec.js'),
  ],
  logLevel: 'trace',
  outputDir: path.join(__dirname, 'tests/e2e/logs'),
  bail: 0,
  waitforTimeout: 1000,
  framework: 'mocha',
  specFileRetries: 1,
  specFileRetriesDeferred: false,
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 30000,
  },
};
